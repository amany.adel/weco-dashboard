
/**
 * Module dependencies.
 */
var express = require('express')
var http = require('http')
var path = require('path')
var app = express()
var session = require('express-session')
var bodyParser = require('body-parser')
var methodOverride = require('method-override')
var errorhandler = require('errorhandler')
var morgan  = require('morgan')
var config = require('./config.js')
var url = require('url')
var ejs = require('ejs') 

const server = require("http").createServer(app);
const socket = require("socket.io");
const io = socket.listen(server);

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')

var passport = require("passport")
var BearerStrategy = require('passport-http-bearer').Strategy







var signedResponse = function(req, res, next) {
    res.header("X-Powered-By", "weco.com")
    res.header("X-Server", "weco") 
    next()
}

var allowCrossDomain = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
}


app.disable('etag')

// all environments
app.set('port', 3003)
app.use(express.static(__dirname + '/public'));

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(methodOverride())
app.use(signedResponse)
app.use(allowCrossDomain)

const hour = 3600000
const expires  = new Date(Date.now() + hour)

app.use(session({
  secret: 'secret Session Key',
  // resave: false,
  // saveUninitialized: true,
  // cookie: { secure: true }
  // expires :expires ,
  // maxAge:hour,
  resave: true,
  saveUninitialized: false,
  cookie: { maxAge: hour, expires :expires }
}))

app.get("/" , (req , res)=>{
res.render('index')
})

if ('development' == app.get('env')) {
  app.use(errorhandler())
}

server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'))
})


/****************************  Weco Custome Modules   ****************************/
var authenticate = require('./routes/auth')
const helperFunction = require('./lib/helperFunctionWorking.js')

const admin=require('./routes/authAdmin')
const adminMiddlware=require('./middlware/adminAuthMiddlware.js');






/*************************************** Weco Methods *********************************/

/*********************** Authentication */
app.get('/auth/get', authenticate.get)


/*********************** Admin Authentication */

// ***************** Login ************************
app.get('/weco/login', admin.getLogin)

app.post('/weco/login', admin.postLogin)
// ***************** End Login ************************

// ***************** Crud Pkg Item ************************
app.get('/weco/pkgItem',adminMiddlware, admin.getPagePkgItem)
app.post('/weco/deletePkgItem',adminMiddlware, admin.deletePkgItem)
app.post('/weco/insertPkgItem',adminMiddlware, admin.addPkgItem)
// ***************** End Pkg Item ************************

// ***************** Crud Pkg Content ************************
app.get('/weco/pkg',adminMiddlware, admin.getPagePkg)
app.post('/weco/deletePkg',adminMiddlware, admin.deletePkg)
app.post('/weco/infoPkg',adminMiddlware, admin.detailsPkg)
app.post('/weco/Pkg',adminMiddlware, admin.Pkg)
app.post('/weco/updatePkg',adminMiddlware, admin.updatePkg)


// ***************** End Pkg Item ************************

// ***************** Crud Weco Admin ************************
app.get('/weco/admin',adminMiddlware, admin.getPageAdmins)
app.post('/weco/deleteAdmin',adminMiddlware, admin.deleteAdmin)
app.post('/weco/insertAdmin',adminMiddlware, admin.addAdmin)
app.post('/weco/updateAdmin',adminMiddlware, admin.updateAdmin)
app.post('/weco/showPassword',adminMiddlware, admin.showPassword)
app.post('/weco/infoAdmin',adminMiddlware, admin.infoAdmin)

// ***************** End Weco Admin ************************









/*************************************************************************************/

/** End list tables */
app.get('*', function(req, res, next) {
  var err = new Error();
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
	console.log(err.status)
	if(err.status !== 404)
		return next();

	res.status(404).json({ error: true, errorCode: "RouteNotFound" })
});


// ******************* Socket *********************
io.on("connection", function (client) {
  console.log("client connected ");

  // ******* get all package items
  client.on("allPkgItem", async () => {
    pkgItem = await admin.getPkgItem('pkg_item','is_deleted',0);
    io.emit("allPkgItem", pkgItem);
  });

  // ******* get all weco admins
  client.on("admins", async () => {
    admins = await admin.getAdmin('admin_user','is_deleted',0);
    io.emit("admins", admins);
  });

  // ******* get all package
  client.on("allPkg", async () => {
    allPkg = await admin.getPkg('package','is_deleted',0);
    io.emit("allPkg", allPkg);
  });
});
// *************************************************