/*var mysql  = require("../lib/mysql.js")
var config = require('../config.js')
var async  = require("async")
var helper =  require('../lib/helper.js')
var memcachedS = require('memcached')
var memcached  =  new memcachedS()
var url = require('url')


exports.getToken = function(req, res, next){
	if (!memcached)
		return res.json({ error: true, errorCode: "Message", response: "Memcached connect failed" })
	
	var qs = url.parse(req.url, true)
	var qs_params = qs.query

	var grand_type = req.body.grand_type || qs_params.grand_type || null
	var refresh_token = req.body.refresh_token || qs_params.refresh_token || null
	var client_id = req.body.client_id || qs_params.client_id || null
	var client_secret = req.body.client_secret || qs_params.client_secret || null
	
	if(client_id == null) return res.status(412).json({ error: true, errorCode: "Message", response: "Client ID parameter missing." })
	if(client_secret == null) return res.status(412).json({ error: true, errorCode: "Message", response: "Client Secret parameter missing." })
	if(grand_type == null) return res.status(412).json({ error: true, errorCode: "Message", response: "Grand Type parameter missing." })

	mysql.query("SELECT * FROM users WHERE identifier = ? ", client_id, function(err, user){
		if(err) 
			return res.status(412).json({ error: true, errorCode: "ConnectionError", response: "Connection Error. Try again later." })
			
		if(!user.length) 
			return res.status(412).json({ error: true, errorCode: "UserNotFound", response: "No user with client_id you requested." })
		
		if(client_secret != user[0].client_secret) 
			return res.status(412).json({ error: true, errorCode: "Message", response: "Enter a valid Client Secret" })
		
		if(grand_type == "refresh_token" && user[0].refresh_token != refresh_token) 
			return res.status(412).json({ error: true, errorCode: "Message", response: "Invalid Refresh Token" })
			
		if(grand_type == "refresh_token"){
			memcached.get(client_id, function(err, data) {
				if (err || data == false || data == undefined)
					return setUserDataFromDb(user, client_id, function(err, token){
						if(err){ 
							res.status(412).json({ error: true, errorCode: "GetTokenError", response: "Error getting token" })
						} else{
							res.json({ error: false, token: token })
						}
					})
				
				memcached.get(data, function(err, data) {
					if (err || data == false || data == undefined)
						return setUserDataFromDb(user, client_id, function(err, token){
							if(err){ 
								res.status(412).json({ error: true, errorCode: "GetTokenError", response: "Error getting token" })
							} else{
								res.json({ error: false, token: token })
							}
						})
						
					memcached.del(data.token.access_token, function(err){
					
						data.token.access_token = helper.uid(36)
						
						saveToSessionStore(data.token.access_token, data, client_id, function(err){
							if(err) return res.status(412).json({ error: true, errorCode: "GetTokenError", response: "Error getting token" })
							
							res.json({ error: false, token: token })
						})
					})
				})
			})
		} else if(grand_type == "bearer"){
				
			setUserDataFromDb(user, client_id, function(err, token){
				if(err) return res.status(412).json({ error: true, errorCode: "GetTokenError", response: "Error getting token" })
							
				res.json({ error: false, token: token })
			})
		} else{
			res.status(412).json({ error: true, errorCode: "Message", response: "Unknown Grand Type." })
		}
	})
}

var setUserDataFromDb = function(user, client_id, cb){

	var date = new Date()
	var token = {}
	token.access_token = helper.uid(36)
	token.expires_in = 7200000
	token.refresh_token = user[0].refresh_token
	token.token_type = "bearer"
	
	var userData = {}
	userData.UserId = user[0].id
	userData.Email = user[0].username
	userData.FirstName = user[0].firstname
	userData.LastName = user[0].lastname
	userData.Timezone = user[0].timezone
	userData.Balance = user[0].current_balance
	userData.Address = user[0].address
	userData.Web = user[0].web
	userData.Title = user[0].title
	userData.Industry = user[0].industry
	userData.Invitations = user[0].invitations
	userData.HasCCard = user[0].cc_profile
	userData.CCProfileId = user[0].cc_profile_id
	userData.SubAmount = user[0].sub_amount
	userData.SubStatus = user[0].sub_status
	userData.boxesSuspended = user[0].boxes_suspended
	userData.UserIdentifier = user[0].identifier
	userData.Status = user[0].status
	userData.Verified = user[0].is_verified
	userData.loggedIn = true;
	
	userData.token = token
	
	saveToSessionStore(token.access_token, userData, client_id, function(err){
		if(err) return cb(err)
		
		cb(null, token)
	})
}

var saveToSessionStore = function(accessToken, userData, clientID, cb){
	memcached.set(accessToken, userData, 14400, function(err){
		if (err) return cb(err)
		
		memcached.set(clientID, accessToken, 14400, function(err){
			if (err) return cb(err)
				
			cb(null)
		})
	})
}*/
