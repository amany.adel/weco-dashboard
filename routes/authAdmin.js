var mysql = require("../lib/mysql.js")
var config = require('../config.js')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
const helperFunction = require('../lib/helperFunctionWorking.js')

const now = new Date();
const date = require('date-and-time');






exports.getLogin = (req, res) => {
        res.render("login/login");
    }
    /*--------------- Post Login --------------------------*/
exports.postLogin = async(req, res) => {
    try {
        const userName = req.body.userName;
        const password = req.body.password;

        const adminData = await helperFunction.getThreeCol('admin_user', 'id', 'user_name', 'pass', 'user_name', userName);
        if (!adminData) {
            res.send('unable to login');
            throw new Error('unable to login no data');

        }
        console.log("data is " + adminData)

        const isMatch = await bcrypt.compare(password, adminData[0].pass);
        console.log(isMatch);
        if (!isMatch) {
            res.send('unable to login');
            throw new Error('unable to login ');

        }

        const token = await jwt.sign({
            user_name: adminData[0].user_name
        }, 'privateTokenKey');
        console.log(token)

        let promiseToken = await helperFunction.updateOneCol('admin_tokens', 'token', token, 'admin_id', adminData[0].id)

        req.session.admin = adminData[0];
        req.session.token = token;

        console.log(req.session.admin)
        console.log(req.session.token)
        res.send('sucess');
        // res.render()

    } catch (e) {
        res.status(400).send(e);
        console.log(e);
    }

}


/*--------------- get package items --------------------------*/
exports.getPkgItem=async(req,res)=>{
    const pkgItem= await helperFunction.selectAllCol('pkg_item','is_deleted',0);
    console.log(pkgItem );
    return pkgItem;
}

/*--------------- get page package items --------------------------*/
exports.getPagePkgItem=async(req,res)=>{
    const pkgItem= await helperFunction.selectAllCol('pkg_item','is_deleted',0);
    console.log(JSON.stringify(pkgItem ));
    res.render('index',{data:pkgItem});
}

/*--------------- delete package item --------------------------*/
exports.deletePkgItem=async(req,res)=>{
    const itemId=req.body.itemId;
    const pkgItemUpdated= await helperFunction.updateOneCol('pkg_item','is_deleted',1,'id',itemId)
    console.log('item is deleted');
    // const pkgItem= await helperFunction.selectAllCol('pkg_item','is_deleted',0);
    // console.log(JSON.stringify(pkgItem ));
    // res.render('index',{data:pkgItem});
    res.send('item is deleted');
    
}

/*--------------- add package item --------------------------*/
exports.addPkgItem=async(req,res)=>{
    let itemName=req.body.itemName;
    let dateNow=date.format(now, 'YYYY-MM-DD HH:mm:ss'); 
    console.log(dateNow);
    const pkgItemInserted= await helperFunction.insertTwoCol('pkg_item','name','created_on',itemName,dateNow) ;
    console.log('item is inserted');
    res.send('item is inserted');
    
}

/*--------------- get page package Content --------------------------*/
exports.getPagePkg=async(req,res)=>{

    res.render('package');
}

/*--------------- get package --------------------------*/
exports.getPkg=async()=>{
    const pkg= await helperFunction.selectAllCol('package','is_deleted',0);
    console.log(pkg );
    return pkg;
}

/*--------------- delete package item --------------------------*/
exports.deletePkg=async(req,res)=>{
    let pkgId=req.body.pkgId;
    let pkgUpdated= await helperFunction.updateOneCol('package','is_deleted',1,'id',pkgId)
    console.log('package is deleted');
   
    res.send('package is deleted');
    
}

/*--------------- get package --------------------------*/
exports.Pkg=async(req,res)=>{
    let pkgId=req.body.pkgId;
    const pkg= await helperFunction.selectAllCol('package','id',pkgId);
    console.log(pkg );
    res.send(pkg);
}

/*--------------- get detail package --------------------------*/
exports.detailsPkg=async(req,res)=>{
    let pkgId=req.body.pkgId;
    console.log('pkg id is '+ pkgId);
    const pkg= await helperFunction.selectJoinPkg(pkgId)
    console.log(pkg );
    res.send(pkg);
}
/*--------------- update package --------------------------*/
exports.updatePkg=async(req,res)=>{
    let pkgIdUpdate=req.body.pkgIdUpdate;
    let namePkgUpdate=req.body.namePkgUpdate;
    let descriptionPkgUpdate=req.body.descriptionPkgUpdate;
    let pricePkgUpdate=req.body.pricePkgUpdate;
    let numOfRegPkgUpdate=req.body.numOfRegPkgUpdate;

    const pkgItemUpdated= await helperFunction.updateFourCol(
        'package','name','description','price','number_of_reg',
        namePkgUpdate,descriptionPkgUpdate,pricePkgUpdate,numOfRegPkgUpdate,
        'id',pkgIdUpdate)
    console.log('pkg is updated');
    
    res.send('pkg is updated');
}



/*--------------- get page admins --------------------------*/
exports.getPageAdmins=async(req,res)=>{

    res.render('admins');
}

/*--------------- get admin --------------------------*/
exports.getAdmin=async()=>{
    const admins= await helperFunction.selectAllCol('admin_user','is_deleted',0);
    console.log(admins );
    return admins;
}

/*--------------- delete admin --------------------------*/
exports.deleteAdmin=async(req,res)=>{
    let adminId=req.body.adminId;
    const pkgItemUpdated= await helperFunction.updateOneCol('admin_user','is_deleted',1,'id',adminId)
    console.log('admin is deleted');
    
    res.send('admin is deleted');
}

/*--------------- show password of admin --------------------------*/
exports.showPassword=async(req,res)=>{
    const adminId=req.body.adminId;
    const getPassword= await helperFunction.getCol('admin_user','pass','id',adminId)
    console.log(getPassword);
    
    // res.send('admin is deleted');
}

/*--------------- add admin --------------------------*/
exports.addAdmin=async(req,res)=>{
    let nameAdmin=req.body.nameAdmin;
    let phoneAdmin=req.body.phoneAdmin;
    let addressAdmin=req.body.addressAdmin;
    let jobTitleAdmin=req.body.jobTitleAdmin;
    let detailsAdmin=req.body.detailsAdmin;
    let snnAdmin=req.body.snnAdmin;
    let userNameAdmin=req.body.userNameAdmin;
    let passwordAdmin=req.body.passwordAdmin;
    let createdOn=date.format(now, 'YYYY-MM-DD HH:mm:ss');

    let hashPassword=await bcrypt.hash(passwordAdmin,12);
    console.log('password after hash is '+ hashPassword)

    let adminInserted= await helperFunction.insertNineCol(
        'admin_user',
        'name','phone','address','job_title','description','ssn','user_name','pass','created_on',
        nameAdmin,phoneAdmin,addressAdmin,jobTitleAdmin,detailsAdmin,snnAdmin,userNameAdmin,hashPassword,createdOn) ;
    console.log('admin is inserted');
    res.send('admin is inserted');
    
}


/*--------------- get admin info --------------------------*/
exports.infoAdmin=async(req,res)=>{
    let adminId=req.body.adminId;
    const adminInfo= await helperFunction.selectAllCol('admin_user','id',adminId);
    console.log(adminInfo );
    res.send(adminInfo);
}

/*--------------- update admin --------------------------*/
exports.updateAdmin=async(req,res)=>{
    let adminIdUpdate=req.body.adminIdUpdate;
    let nameAdminUpdate=req.body.nameAdminUpdate;
    let phoneAdminUpdate=req.body.phoneAdminUpdate;
    let addressAdminUpdate=req.body.addressAdminUpdate;
    let jobTitleAdminUpdate=req.body.jobTitleAdminUpdate;
    let detailsAdminUpdate=req.body.detailsAdminUpdate;
    let snnAdminUpdate=req.body.snnAdminUpdate;
    let userNameAdminUpdate=req.body.userNameAdminUpdate;
    let updatedOn=date.format(now, 'YYYY-MM-DD HH:mm:ss');

    let adminUpdated= await helperFunction.updateEightCol(
        'admin_user',
        'name','phone','address','job_title','description','ssn','user_name','updated_on',
        nameAdminUpdate,phoneAdminUpdate,addressAdminUpdate,jobTitleAdminUpdate,detailsAdminUpdate,snnAdminUpdate,userNameAdminUpdate,updatedOn,'id',adminIdUpdate) ;
    console.log('admin is updated');
    res.send('admin is updated');
    
}
