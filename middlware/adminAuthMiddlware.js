const jwt = require('jsonwebtoken');
const helperFunction = require('../lib/helperFunctionWorking.js')
var mysql = require("../lib/mysql.js")
var config = require('../config.js')



const auth = async (req,res,next)=>{
    try{
        console.log('in auth');
        console.log(req.session)
        if(!req.session.token)
        {
            res.render("login/login");
            throw new Error('plz,login'); 
        }
        console.log(req.session.token)
        const decoded = jwt.verify(req.session.token,'privateTokenKey'); // == return username ==  //
        console.log(decoded);


        const user = await helperFunction.getThreeCol('admin_user', 'id', 'user_name', 'job_title', 'user_name', decoded.user_name);
        
        if(!user){
            // throw new Error('plz,login');
            res.render("login/login");      
        }

        req.session.admin = user ;
        req.session.token = req.session.token ;
        next();
        
    }catch (error){
        console.log(error);
        res.status(401).send({ 'error' : 'please log in '});
    }
    
}
module.exports = auth