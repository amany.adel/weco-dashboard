var strftime = require('strftime')
var config =  require('../config.js')
var memcachedStore = require('memcached')
var memcached  =  new memcachedStore([ config.__MEMCACHED_HOST__ ]) 

exports.randomString = function(stringLength){
    var string = "s";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < stringLength; i++ )
        string += possible.charAt(Math.floor(Math.random() * possible.length));

    return string;
}

exports.strongpassrandom = function(stringLength){
    var string = "";
    var possiblechU = "ABCDEFGHJKLMNOPQRSTUVXYZ"
	var possiblechL = "abcdefghjklmnopqrstuvwxyz"
	var possiblenr = "023456789"

    for( var i=0; i < stringLength; i++ ){
		if( i%3==0){
			string += possiblenr.charAt(Math.floor(Math.random() * possiblenr.length))
		} else if( i%3==1){
			string += possiblechU.charAt(Math.floor(Math.random() * possiblechU.length))
		} else {
			string += possiblechL.charAt(Math.floor(Math.random() * possiblechL.length))
		}
	}

    return string;
}


exports.ucFirst = function(string){
    return string.charAt(0).toUpperCase() + string.slice(1)
}

exports.ucWords = function(string){
    return string.replace(/\w\S*/g, function(txt){ return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() } )
}


exports.saveSession = function(tkn, userData, cb){
	memcached.set(tkn, userData, 14400, function(err){
		if (err) return cb(err)
		cb(null)
	})
}


exports.uid = function(len) {
	var buf = []
		, chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
		, charlen = chars.length

	for (var i = 0; i < len; ++i) {
		buf.push(chars[getRandomInt(0, charlen - 1)])
	}
	

    return buf.join('')
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min
}

exports.create_uuid = function(){
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8)
		return v.toString(16)
	})
	return uuid
}

exports.saveProcessId = function(process_id, data){
	memcached.set(process_id, data, 3600, function(err){})
}

exports.getProcessId = function(process_id, cb){
	memcached.get(process_id, function(err, data){
		if (err) return cb(err)
		
		if(data == false || data == undefined)
			return cb(null, null)
		
		cb(null, data)
	})
}

exports.sendEmail = function(params, contactpage){

	var nodemailer = require("nodemailer")
	var config = require("../config.js");

	var transport = nodemailer.createTransport("SMTP", {
	    host: config.__SMTP_HOST__,
	    port: config.__SMTP_PORT__,
	    secureConnection: false,

	    auth: {
	        user: config.__SMTP_USER__,
	        pass: config.__SMTP_PASS__
	    }
	});
	
	var bcc_email = ( contactpage == 1 ) ? config.__CONTACT_BCC_EMAIL__ : config.__BCC_EMAIL__

	var mailOptions = {
        from: params.from,
        to: params.to,
        cc: params.cc,
        bcc: bcc_email ,
        replyTo: params.replyTo,
        subject: params.subject,
        html: params.html,
        text: params.text,
        attachments: params.attachments
    }

    transport.sendMail(mailOptions, function(error, response){

        if(error)
        {
            console.log(error);
        }
        else
        {
            console.log("Message sent: " + response.message);
        }

        transport.close(); // shut down the connection pool, no more messages
    });
}

exports.utcDate = function(){
	return new Date(new Date().toUTCString().substr(0, 25));
}


